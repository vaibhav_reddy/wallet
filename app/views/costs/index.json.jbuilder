json.array!(@costs) do |cost|
  json.extract! cost, :id, :date, :category, :amount
  json.url cost_url(cost, format: :json)
end
