json.array!(@incomes) do |income|
  json.extract! income, :id, :date, :category, :amount
  json.url income_url(income, format: :json)
end
