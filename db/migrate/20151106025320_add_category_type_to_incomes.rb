class AddCategoryTypeToIncomes < ActiveRecord::Migration
  def change
    add_column :incomes, :category_type, :string
  end
end
