class AddCategoryTypeToCosts < ActiveRecord::Migration
  def change
    add_column :costs, :category_type, :string
  end
end
