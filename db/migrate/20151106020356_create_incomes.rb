class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.date :date
      t.string :category
      t.integer :amount

      t.timestamps
    end
  end
end
